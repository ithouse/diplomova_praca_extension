

var UserManager = {

    users : [],

    get_users : function () {
        return this.users;
    },

    add_user : function (user) {
         this.users.push(user);
    },


};


var User = {

    create_user : function (name, id, priority, color) {
        this.name = name;
        this.id = id;
        this.priority = priority;
        this.color = ColorManager.get_random_color();
        return this;
    },


};

