

var CoreManager = {

    surface : null,
    surfaceModel : null,
    surfaceModel_document : null,
    data_map : {},


    initialize_map : function(){

        let data = CoreManager.surface_handler.getter.get_surface_model_document().data.data;

        let counter = 1 ;
        let init_name = "init";
        for(var i = 0; i < data.length; i++){
           // console.log(typeof data[i]);
            if (typeof data[i] === 'string'){
                CoreManager.data_map[counter] = new Identifier(
                    init_name + "_"+ counter,
                    data[i],
                    counter,
                    counter,
                    null)
                counter +=1;
                continue;
            }
            if(typeof data[i] === 'object' && !data[i].internal && typeof data[i][0] !== 'undefined'){
                CoreManager.data_map[counter] = new Identifier(
                    init_name + "_"+ counter,
                    data[i][0],
                    counter,
                    counter,
                    data[i][1]);
                counter +=1;
            }


        }
       /// console.log("xxMAPxx",CoreManager.data_map,data);

    },

    surface_handler : {

        setter : {
            set_base : function () {
                CoreManager.target =  ve.init.target;
                }
,
        },
        getter : {
            get_surface : function(){
                CoreManager.target.getSurface();

            },
            get_surface_model : function () {
                return CoreManager.target.getSurface().getModel();
            },

            get_surface_model_document : function () {
                return CoreManager.target.getSurface().getModel().getDocument();
            },
        },
    },

    editor_handler : {

        getter : {

            get_editor_data : function () {
                return CoreManager.getter.get_surface_model().getData();
            },
        },


    },






};