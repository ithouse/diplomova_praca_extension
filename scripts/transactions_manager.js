var Identifier  = function (siteID, character, counter, offset, annotation) {
  this.siteID = siteID;
  this.character = character;
  this.counter = counter;
  this.offset = offset;
  this.annotationIndex = annotation;

};


var TransactionManager = {

    local_transactions : [],
    counter : 1,
    server_transactions : [],
    char_identifiers : [],
    custom_mapping : {},
    delete_buffer : {},
    setter : {

    },

    getter : {
        get_transaction_json : function (transaction_operations) {

            let to = transaction_operations;
            let insert_characters = [];
            let insert_characters_hash = [];
            let remove_characters = [];
            let remove_characters_hash = [];
            let remove_characters_map_positions = [];
            let do_remove = false;
            let do_insert = false;

            if( to[1].insert && to[1].insert.length > 0){
                insert_characters = to[1].insert;
                insert_characters_hash = to[1].insert;
                do_insert = true;}

            if( to[1].remove && to[1].remove.length > 0){
                remove_characters = to[1].remove;
                remove_characters_hash = to[1].remove;
                remove_characters_map_positions = TransactionManager.getter.getMapPositions(to[0].length,to[1].remove.length);
                do_remove = true;}



            return {
                'side_id' : SocketServices.getter.get_socket().id,
                'side_last_counter' : TransactionManager.counter,
                'type' : to[1].type,
                'method' : to[1].method,
                'annotationIndex' : to[1].index,
                'length' : to[2].length,
               // 'annotation' : CoreManager.surface_handler.getter.get_surface_model_document().getStore().hashStore[to[1].index],
                'transaction_type' :to[1].type,
                'offset' : to[0].length,
                'do_insert' : do_insert,
                'do_remove' : do_remove,
                'remove_characters' : remove_characters,
                'remove_characters_hash' : remove_characters_hash,
                'remove_characters_map_positions' : remove_characters_map_positions,
                'insert_characters' : insert_characters,
                'insert_characters_hash' : insert_characters_hash
            }
        },


        getMapPositions : function (offsetStart, length) {

            let map = CoreManager.data_map;
            let char_from_left = null;
            let char_from_right = null;
            let positions = [];

          //  console.log("VOLAM GET ,a toto je map", map);
            for(let position in map) {
                console.log("positions", position);
                if (map[position].offset >= offsetStart && map[position].offset < offsetStart + length) positions.push(position);
              //  if (map[position].offset >= offsetStart && map[position].offset >= offsetStart + length) break;

            }
                console.log("Toto je position", positions, offsetStart, length);
            return positions;
        },

        get_mapping_positions_for_insert : function (transaction) {

            let tr_offset = transaction.offset;
            let map = CoreManager.data_map;
            let char_from_left = null;
            let char_from_right = null;
            let positions = [];


            console.log("Tieto pozicie offset a dlzka mapy+1", (Number(transaction.offset) ), (Number(Object.keys(map).length ))+1);

            if(transaction.offset == 1) { //If inserting on end of document
                for (position in map) {
                    if (map[position].offset === 1) {
                        char_from_left = 0;
                        char_from_right = position;

                        transaction.insert_characters.forEach(function (character) {
                            console.log("vkladam NA zaciatok", char_from_left);
                            let new_position = (Number(char_from_left) + Number(char_from_right)) / 2;
                            positions.push(new_position);
                            char_from_left = new_position;
                        });

                    }
                    map[position].offset +=1;
                }
                return positions;
            }

            console.log("Vkladam na koniec", "1111111");
                if (Number(transaction.offset) == (Number(Object.keys(map).length ))+1) { //If inserting on end of document
                    for (position in map) {
                        if (map[position].offset === tr_offset-1) {
                            char_from_left = position;

                            transaction.insert_characters.forEach(function (character) {
                                console.log("Vkladam na koniec", char_from_left);
                                let new_position = Number(char_from_left )+1;
                                positions.push(new_position);
                                char_from_left = new_position;
                            });
                            return positions;
                        }
                    }
                }
                for (position in map) {
                    if (map[position].offset === tr_offset - 1) char_from_left = position;
                    if (map[position].offset === tr_offset) char_from_right = position;
                    if (char_from_right != null && char_from_left != null) {
                        transaction.insert_characters.forEach(function (character) {
                            console.log("MEDZI tieto pozicie vkladam", char_from_left, char_from_right);
                            let new_position = (Number(char_from_left) + Number(char_from_right)) / 2;
                            positions.push(new_position);
                            char_from_left = new_position;
                        });
                        char_from_right = null;
                    }

                    if (map[position].offset >= tr_offset) map[position].offset += transaction.insert_characters.length;
                }

                return positions;
            }
    },

    factory : {
        create_newFromInsertion : function (document, offset, data) {
            return ve.dm.TransactionBuilder.static.newFromInsertion(document, offset , data );
        },

        create_newFromRemoval : function (document, rangeSTART,rangeEND, data) {
            let range = new ve.Range( rangeSTART, rangeEND );
            return ve.dm.TransactionBuilder.static.newFromRemoval(document, range, [data] );
        },

        /**
         * Generate a transaction that annotates content.
         *
         * @static
         * @method
         * @param {ve.dm.Document} doc Document in pre-transaction state
         * @param {ve.Range} range Range to annotate
         * @param {string} method Annotation mode
         *  - `set`: Adds annotation to all content in range
         *  - `clear`: Removes instances of annotation from content in range
         * @param {ve.dm.Annotation} annotation Annotation to set or clear
         * @return {ve.dm.Transaction} Transaction that annotates content
         */
        // ve.dm.TransactionBuilder.static.newFromAnnotation = function ( doc, range, method, annotation ) {
        // var covered, annotatable,
        //     txBuilder = new ve.dm.TransactionBuilder(),
        //     data = doc.data,
        //     index = doc.getStore().index( annotation ),
        //     i = range.start,
        //     span = i,
        //     on = false,
        //     insideContentNode = false,
        //     ignoreChildrenDepth = 0;

        create_newFromAnnotation : function ( document, rangeSTART,rangeEND, method, annotation ) {
            let range = new ve.Range( rangeSTART, rangeEND );


            return ve.dm.TransactionBuilder.static.newFromAnnotation2( document, range, method, annotation );
        },
    },


    storage : {
        add_to_local_transactions : function (transaction) {
            TransactionManager.local_transactions.push(transaction);



           // console.log("TAKyto transaction posielam PRO",transaction);
            SocketServices.SocketSender.send_transactions(transaction);
          //  TransactionManager.local_transactions = []; //TODO managment of query

        },
        update_dataMap : function (transaction) {

            if(transaction.do_remove) { //TODO dorobit nejaky slice ,aby to bral od offsetu hore len
                for (let position in CoreManager.data_map) {
                    if(position > transaction.remove_characters_map_positions[transaction.remove_characters_map_positions.length-1]){
                        CoreManager.data_map[position].offset -= transaction.remove_characters_map_positions.length;
                    }
                }
                console.log("vymazavam tieto pozicie", transaction.remove_characters_map_positions);
                transaction.remove_characters_map_positions.forEach(function (position) {
                    delete CoreManager.data_map[position];
                })
                
            }


            if(transaction.do_insert) {
                let offset_plus = 0;
                let positions = [];
                let identifiers = [];
                transaction.insert_characters.forEach(function (c) {
                    let identifier = new Identifier(
                        transaction.uniqueID + "_" + offset_plus,
                        c,
                        transaction.counter,
                        transaction.offset + offset_plus,
                        null); //TODO rozlisit annotacie
                    offset_plus += 1;
                    //TODO vypocitat poziciu delenim
                    identifiers.push(identifier);

                });
                positions = TransactionManager.getter.get_mapping_positions_for_insert(transaction);
               // console.clear();
                console.log("TOTO JE POSITIONS NA INSERT," , positions);
                for(let i=0 ; i<positions.length;i++){
                    CoreManager.data_map[positions[i]] = identifiers[i];
                }
                //TODO spravit pre viac znakov naraz --

            }
           // console.clear();
            console.log("Procesujem UPDATE MAP ", CoreManager.data_map);
        },
        edit_offsets : function (start_offset,operation, length) {

        }
    },

    handler : {

        process_remove: function (transaction_json) {
            //TODO najskor asi update mapy.ak je mozny spravime transakciu

            let transaction = TransactionManager.factory.create_newFromRemoval(
                doc,
                transaction_json.offset,
                transaction_json.offset + transaction_json.remove_characters.length,
                transaction_json.remove_characters);
            transaction.by_server = true;
            TransactionManager.handler.add_transaction_to_queue(transaction);
            TransactionManager.storage.update_dataMap(transaction_json);

        },
        process_insert: function (transaction_json) {
            //TODO najskor asi update mapy.ak je mozny spravime transakciu
            let transaction = TransactionManager.factory.create_newFromInsertion(
                doc,
                transaction_json.offset,
                transaction_json.insert_characters);
            transaction.by_server = true;
            TransactionManager.handler.add_transaction_to_queue(transaction);
            TransactionManager.storage.update_dataMap(transaction_json);

        },
        add_transaction_to_queue: function (transaction) {
            TransactionManager.server_transactions.push(transaction);
            //TODO : podmienka kedy sa transakcia pouzije


            // if(tr.do_remove){
            //
            //     tr.remove_characters.forEach( function (character_) {
            //         let character = character_;
            //         let char_identifier = new Identifier(
            //             transaction.side_id,
            //             character,
            //             transaction.side_last_counter);
            //         TransactionManager.char_identifiers.push(char_identifier);
            //     });
            // }
            //
            //
            // if(tr.do_insert){
            //
            //     tr.insert_characters.forEach( function (character_) {
            //         let character = character_;
            //         let char_identifier = new Identifier(
            //             transaction.side_id,
            //             character,
            //             transaction.side_last_counter);
            //         TransactionManager.char_identifiers.push(char_identifier);
            //     });
            // }
            // let  char_identifier = new Identifier(
            //     transaction.side_id,
            //     character,
            //     transaction.side_last_counter);
            //
            // if(char_identifier in TransactionManager.char_identifiers){
            //
            // }else{
            // TransactionManager.char_identifiers.push(char_identifier);}

            if (true) {
                CoreManager.surface_handler.getter.get_surface_model().change(transaction);
            }
        },
        get_staging_transactions: function () {
            return CoreManager.surface_handler.getter.get_surface_model().getStagingTransactions();
        },
        apply_all_transactions: function () {
            CoreManager.surface_handler.getter.get_surface_model().applyAllStaging();

            // let trr = TransactionManager.factory.create_newFromInsertion(
            //     CoreManager.surface_handler.getter.get_surface_model_document(),
            //     5,
            //     "<div>11112</div>"
            //
            // );
            // trr.by_server = true;
            // CoreManager.surface_handler.getter.get_surface_model().change(trr);
            // CoreManager.surface_handler.getter.get_surface_model().applyAllStaging();
            // console.log("APLIKUJEM TR JSON", trr);
        },
    }
};