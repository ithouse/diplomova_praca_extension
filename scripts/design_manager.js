

DomManager = {


    render_toolbar : function (clients) {

        var main_toolbar = document.getElementsByClassName('oo-ui-toolbar-bar');

        var main_toolbar_parent = document.getElementsByClassName('oo-ui-toolbar-bar')[0].parentNode;


        var my_toolbar = document.getElementById('my_toolbar');
        if(my_toolbar !== null){
            my_toolbar.innerHTML = "";}
        else{
            my_toolbar = document.createElement('div');
            my_toolbar.html = "";
            my_toolbar.id = 'my_toolbar';
        }


        for (let clientID in clients){

            var name = clients[clientID].name === "default" ? clientID : clients[clientID].name;
            var txt = document.createTextNode(name.split('')[0]);
            var el = document.createElement('div');
            el.className = 'user_box';
            el.appendChild(txt);
            el.style.textAlign = "center";
            el.style.justifyContent = 'center';
            el.style.backgroundColor = clients[clientID].color;
            el.setAttribute('title', clients[clientID].name);
            my_toolbar.appendChild(el);

        }
      //  console.log("VOLAM TOOOLBAR",my_toolbar,clients);

        main_toolbar_parent.insertBefore(my_toolbar, main_toolbar[0]);


    },

    render_cursor_el : function (x,y,el,socketID,color) {
        var cursor_el = document.getElementById(socketID);
        if(cursor_el === null){
            cursor_el = document.createElement('div');
            cursor_el.className = 'cursor_box';
            cursor_el.id = socketID;
            cursor_el.title = socketID;
            cursor_el.style.backgroundColor = color}

        cursor_el.style.left = x +'px';
        cursor_el.style.top = y +'px';


        el = ve.init.target.$element;
        console.log("PRIPAJAM NA EL",el,cursor_el,color);
        el[0].appendChild(cursor_el);
    },
};


ColorManager = {


    get_random_color : function () {
      return this.colors[Math.floor(Math.random() * Math.floor(this.colors.length))];
    },


    set_colors : function () {

        this.colors = ["#66FF66",
            "#FF5050",
            "#99FFCC",
            "#66FFFF",
            "#C29696",
            "#FF9999",
            "#FF0000",
            "#3366CC",
            "#223A5E",
            "#800000",
            "#00CC66",
            "#898E8C",
            "#005960",
            "#9C9A40",
            "#D2691E",
            "#F6D155",
            "#92B6D5",
            "#AD5D5D",
            "#999966"];
    },
};