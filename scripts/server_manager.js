
SocketServices = {

    socket : null,
    room : null,
    clients : [],

    setter : {
        set_socket : function (socket) {
            SocketServices.socket = socket;
            SocketServices.SocketListeners.socket = socket;
        },

        set_base : function () {
            SocketServices.SocketListeners.set_create_room();
            SocketServices.SocketListeners.set_add_user();
            SocketServices.SocketListeners.set_cursor_position();
            SocketServices.SocketListeners.set_synchronize_users();
            SocketServices.SocketListeners.set_apply_transactions();

        },

        set_room : function (room) {
            SocketServices.room = room;
        },

        set_clients(clients){
            SocketServices.clients = clients;
        }
    },
    getter : {
        get_socket : function () {
            return SocketServices.socket;
        },
        get_room : function () {
            return SocketServices.room;
        }
    },

    SocketSender : {
        send_transactions : function (transactions) {
         //   console.log("TOTO POSIELAM NA SERVER :" , transactions, SocketServices.room);
            SocketServices.socket.emit('apply_transactions_server', transactions, SocketServices.room);
        },
    },
    SocketListeners : {

        socket : null,
        set_create_room() {
            this.socket.on('set_room', function (room) {
                SocketServices.setter.set_room(room)
            });
        },
        set_add_user: function () {
            let self = this;
            this.socket.on('add_user', function (clientID) {
                let username = mw.config.get('wgUserName') !== null ? mw.config.get('wgUserName') : "default";
                let user = User.create_user(username, clientID, 1, ColorManager.get_random_color());
                let room = SocketServices.getter.get_room();
                self.socket.emit('synchronize_users_server', user, room);
            });
        },
        set_synchronize_users: function () {
            this.socket.on('synchronize_users', function (clients) {
                self.clients = clients;
                SocketServices.setter.set_clients(clients);
                DomManager.render_toolbar(clients);
            });
        },
        set_cursor_position: function () {
            this.socket.on('cursor_position_home', function (x, y, el, socketID, color) {
                DomManager.render_cursor_el(x, y, el, socketID, color);
            });
        },
        set_apply_transactions: function () {

            this.socket.on('apply_transactions_client', function (transactions_) {
              //  console.log("TOTO SU TRANSAKCIE VOLANE na aplly lcient", transactions_);
                doc = CoreManager.surface_handler.getter.get_surface_model_document();

                let transactions = [].concat(transactions_);
                for (let i in transactions) {


                    console.log("server_manager.js,152",
                        "Aplikovanie transackie na zmenu CYKLUS,tranackia[i]:"
                        , transactions[i], "\n");

                    let tr = JSON.parse(transactions[i]);
                    console.log("server_manager.js,152",
                        "Aplikovanie transackie na zmenu CYKLUS,tranackia[i]:"
                            , tr, "\n");

                    let transaction = null;
                    if(tr.type === 'annotatee') {

                        let store = doc.getStore();

                        console.log("STOREA A STORE NA POZICII", store.hashStore
                            , tr.annotationIndex, "\n");

                        let annotation_obj1 = store.hashStore.h3f5e6c6ca235cd0a;

                        let annotation_obj = store.hashStore[tr.annotationIndex];


                        transaction = TransactionManager.factory.create_newFromAnnotation(
                            doc, tr.offset, tr.offset + tr.length, tr.method, tr.annotation);

                        console.log("Transaction", transaction
                            , tr.annotationIndex, "\n", store);

                        transaction.by_server = true;
                        CoreManager.surface_handler.getter.get_surface_model().change(transaction);


                        CoreManager.surface_handler.getter.get_surface_model().applyAllStaging();

                    }
                    //TODO quick fix pozret preco
                    if(tr === null)continue;

                    if (tr.do_remove) {
                        TransactionManager.handler.process_remove(tr);
                    }
                    if (tr.do_insert) {
                        TransactionManager.handler.process_insert(tr);

                    }
                }
                TransactionManager.handler.apply_all_transactions();

            });
        },

    }
};