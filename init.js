
var wait_on_load = function(){
    console.log("IDEM INTERVAL");
    if(typeof ve.init.target.getSurface() !== 'undefined') {
        clearInterval(id);
        console.clear();



        const main_socket = io('http://localhost:3000');
        main_socket.emit('create',window.location.href); //prida do room

        SocketServices.setter.set_socket(main_socket);
        SocketServices.setter.set_base();

        CoreManager.surface_handler.setter.set_base();



        CoreManager.initialize_map();
        ColorManager.set_colors();

        DomEventsManager.set_events();

        // ve.init.Target.prototype.addSurface = function ( dmDoc, config ) {
        //     var surface = this.createSurface( dmDoc, ve.extendObject( { mode: this.getDefaultMode() }, config ) );
        //     this.surfaces.push( surface );
        //     surface.getView().connect( this, {
        //         focus: this.onSurfaceViewFocus.bind( this, surface )
        //     } );
        //     return surface;
        // };
        //
        // ve.init.Target.prototype.createSurface = function ( dmDoc, config ) {
        //     return new ve.ui.Surface( dmDoc, this.getSurfaceConfig( config ) );
        // };

        // TransactionManager.setter.set_base();


        // model dokument get data ,vrati {ve.dm.ElementLinearData} Content data ..takze je to to
        ve.dm.Document.prototype.commit = function ( transaction, isStaging ) {


            let data = CoreManager.surface_handler.getter.get_surface_model_document().data.data;

            let counter = 1 ;
            // for(var i = 0; i < data.length; i++){
            //
            //     if (typeof data[i] === 'string'){
            //       //  CoreManager.data_map[counter] = {'char' : data[i], 'annotation' : null, 'offset' : counter};
            //         counter +=1;
            //         continue;
            //     }
            //     if(typeof data[i] === 'object' && !data[i].internal){
            //         CoreManager.data_map[counter] = {'char' : data[i][0], 'annotation' : data[i][1], 'offset' : counter};
            //        // counter +=1;
            //     }
            //
            //
            // }
            //console.clear();
           console.log( "DATA MAP a transaction"
               //CoreManager.data_map
           ,transaction);
            if(! transaction.by_server ){
            let transaction_json = TransactionManager.getter.get_transaction_json(transaction.operations);
            transaction_json.uniqueID = SocketServices.getter.get_socket().id + "|" + TransactionManager.counter;

            console.log("COMMIT TR JSON", transaction_json);
            TransactionManager.counter +=1;
            let str_json = JSON.stringify(transaction_json);
            TransactionManager.storage.add_to_local_transactions(str_json);
            TransactionManager.storage.update_dataMap(transaction_json);




//                 console.log(_.map(ownerArr, 'pets[0].name'));


            //     var surfaceModel = ve.init.target.getSurface().getModel();
            //     surfaceModel.getDocument().data.data[1][0] = "X";
            //     var text = surfaceModel.getDocument().getData(); //data - data su data , data - store mam store, mozem pri anotaciach
            //
            //     let dt = CoreManager.surface_handler.getter.get_surface_model_document();
            //    // console.log("xDATAx", dt,text);
            //
            // let store  = CoreManager.surface_handler.getter.get_surface_model_document().getStore();
            //     console.log("TOTO JE TRAN,store", transaction_json , store);

            // let surfaceModel = CoreManager.surface_handler.getter.get_surface_model();
            // let fragment1 = surfaceModel.getFragment( new ve.Range( 1, 100 ) ).surface.documentModel.data;
            //
            //     store = new ve.dm.IndexValueStore();
            //         data = new ve.dm.LinearData( store, "KOKOT" );
            //         expectedData = ve.copy( "KOKOT" );
            //         test_Data = CoreManager.surface_handler.getter.get_surface_model_document().prototype.getDataSlice({'start' :10, 'end':20},2);
            //     console.log("TOTO JE dat", test_Data);
            }
            if ( transaction.hasBeenApplied() ) {
                throw new Error( 'Cannot commit a transaction that has already been committed' );
            }
            this.emit( 'precommit', transaction );
            new ve.dm.TransactionProcessor( this, transaction, isStaging ).process();
            this.completeHistory.push( transaction );
            this.storeLengthAtHistoryLength[ this.completeHistory.length ] = this.store.getLength();
            this.emit( 'transact', transaction );
        };


        ve.dm.TransactionBuilder.static.newFromAnnotation2 = function ( doc, range, method, annotationIndex ) {
            var covered, annotatable,
                txBuilder = new ve.dm.TransactionBuilder(),
                data = doc.data,
                index = annotationIndex,

                i = range.start,
                span = i,
                on = false,
                insideContentNode = false,
                ignoreChildrenDepth = 0;


            // console.log("KKKK", index, annotation, doc.getStore());

            // Iterate over all data in range, annotating where appropriate
            while ( i < range.end ) {
                if ( data.isElementData( i ) && ve.dm.nodeFactory.shouldIgnoreChildren( data.getType( i ) ) ) {
                    ignoreChildrenDepth += data.isOpenElementData( i ) ? 1 : -1;
                }
              //  annotatable = !ignoreChildrenDepth && data.canTakeAnnotationAtOffset( i, annotation );
                //TODO toto dam vzdy asi true

                annotatable = true;
                if (
                    !annotatable ||
                    ( insideContentNode && !data.isCloseElementData( i ) )
                ) {
                    // Structural element opening or closing, or entering a content node
                    if ( on ) {
                        txBuilder.pushRetain( span );
                        txBuilder.pushStopAnnotating( method, index );
                        span = 0;
                        on = false;
                    }
                } else if (
                    ( !data.isElementData( i ) || !data.isCloseElementData( i ) ) &&
                    !insideContentNode
                ) {
                    // Character or content element opening
                    if ( data.isElementData( i ) ) {
                        insideContentNode = true;
                    }
                    if ( method === 'set' ) {
                        // Don't re-apply matching annotation
                      //  covered = data.getAnnotationsFromOffset( i ).containsComparable( annotation );
                    } else {
                        // Expect comparable annotations to be removed individually otherwise
                        // we might try to remove more than one annotation per character, which
                        // a single transaction can't do.
                        //covered = data.getAnnotationsFromOffset( i ).contains( annotation );
                    }
                    covered = false;
                    console.log("TOTO JE INDEX",index,covered,annotatable);
                  //  covered = true;
                    if ( ( covered && method === 'set' ) || ( !covered && method === 'clear' ) ) {
                        // Skip annotated content
                        if ( on ) {
                            txBuilder.pushRetain( span );
                            txBuilder.pushStopAnnotating( method, index );
                            span = 0;
                            on = false;
                        }
                    } else {
                        // Cover non-annotated content
                        if ( !on ) {
                            txBuilder.pushRetain( span );
                            txBuilder.pushStartAnnotating( method, index );
                            span = 0;
                            on = true;
                        }
                    }
                } else if ( data.isCloseElementData( i ) ) {
                    // Content closing, skip
                    insideContentNode = false;
                }
                span++;
                i++;
            }
            txBuilder.pushRetain( span );
            if ( on ) {
                txBuilder.pushStopAnnotating( method, index );
            }
            txBuilder.pushFinalRetain( doc, range.end );
            return txBuilder.getTransaction();
        };


        ve.dm.TransactionProcessor.processors.annotate = function ( op ) {

            var target, annotation;
            if ( op.method === 'set' ) {
                target = this.set;
            } else if ( op.method === 'clear' ) {
                target = this.clear;
            } else {
                throw new Error( 'Invalid annotation method ' + op.method );
            }
            console.log("toto je targer" , op, target);
            if ( op.bias === 'start' ) {
                target.push( annotation );
            } else {
                target.remove( annotation );
            }
            // Actual changes are done by applyAnnotations() called from the retain processor
        };

    }
};
var id = setInterval(wait_on_load, 2000);
